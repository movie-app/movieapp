package com.app.movieapp.entity;

public class Movie {

    private int id;
    private String imdb_id;
    private String original_title;
    private String overview;
    private String release_date;
    private Double vote_average;
    private int runtime;

    public Movie() {
    }

    public Movie(int id, String imdb_id, String original_title, String overview, String release_date, Double vote_average, int runtime) {
        this.id = id;
        this.imdb_id = imdb_id;
        this.original_title = original_title;
        this.overview = overview;
        this.release_date = release_date;
        this.vote_average = vote_average;
        this.runtime = runtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public Double getVote_average() {
        return vote_average;
    }

    public void setVote_average(Double vote_average) {
        this.vote_average = vote_average;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", imdb_id=" + imdb_id +
                ", original_title='" + original_title + '\'' +
                ", overview='" + overview + '\'' +
                ", release_date=" + release_date +
                ", vote_average=" + vote_average +
                ", runtime=" + runtime +
                '}';
    }
}
