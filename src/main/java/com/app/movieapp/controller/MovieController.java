package com.app.movieapp.controller;

import com.app.movieapp.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class MovieController {

    @Autowired
    RestTemplate restTemplate;

    private String latestMovies = "https://api.themoviedb.org/3/movie/latest?api_key=d02847d01e1f3147b4eba1afbe0ed1d6";

    private String topRatedMovies = "https://api.themoviedb.org/3/movie/top_rated?api_key=d02847d01e1f3147b4eba1afbe0ed1d6";

    private String upComingMovies = "https://api.themoviedb.org/3/movie/upcoming?api_key=d02847d01e1f3147b4eba1afbe0ed1d6";

    private String popularMovies = "https://api.themoviedb.org/3/movie/popular?api_key=d02847d01e1f3147b4eba1afbe0ed1d6";

    @GetMapping("/getLatestMovies")
    public Movie getLatestMovies(){
        return restTemplate().getForObject(latestMovies, Movie.class);
    }

    @GetMapping("/getTopRatedMovies")
    public Movie getTopRatedMovies(){
        return restTemplate().getForObject(topRatedMovies, Movie.class);
    }

    @GetMapping("/getUpComingMovies")
    public Movie getUpComingMovies(){
        return restTemplate().getForObject(upComingMovies, Movie.class);
    }

    @GetMapping("/getPopularMovies")
    public Movie getPopularMovies(){
        return restTemplate().getForObject(popularMovies, Movie.class);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
