package com.app.movieapp;

import com.app.movieapp.entity.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MovieappApplication {

	private static final Logger log = LoggerFactory.getLogger(MovieappApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(MovieappApplication.class, args);
	}

//	@Bean
//	public RestTemplate restTemplate(RestTemplateBuilder templateBuilder){
//		return templateBuilder.build();
//	}

//	@Bean
//	public CommandLineRunner run(RestTemplate restTemplate) throws Exception{
//		return args -> {
//			//get latest movies
//			Movie latestMovies = restTemplate.getForObject("https://api.themoviedb.org/3/movie/latest?api_key=d02847d01e1f3147b4eba1afbe0ed1d6", Movie.class);
//
//			log.info(latestMovies.toString());
//		};
//	}


}
